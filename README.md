# Seedbox

Makes use of OpenVPN, RPI-Monitor, Transmission and VirusTotal. To provide a complete seedbox experience. Allow users to Monitor the state of the machine, Manage torrents and have them automatically scanned from malware. Providing all logs and downloads directly only allowing transfers at LAN speeds.

# Features

- Malware Scan on Torrent Completion using VirusTotal
- Killswitch on VPN disconnect
- Allow management of drives and IP directly from Web Dashboard
- Remotely accessible torrents status via Transmission-WebUI
- Allow lan access to all completed downloads and Scan Logs through Web UI
- Easily configurable
- No added dependencies

![System Management](images/dashboard.png)
![Files Access](images/centralized.png)

# OS Requirements

- Ubuntu Server 20.04 ARM64
- [Installation and Wifi](https://ubuntu.com/tutorials/how-to-install-ubuntu-on-your-raspberry-pi)
- [Automatic HDD Mounting](https://www.techrepublic.com/article/how-to-properly-automount-a-drive-in-ubuntu-linux/)

# Installation

1. Download and move your OPENVPN `ovpn` config into a prefered dir and edit it as follows (Not provided any OpenVPN Config works)

   - Change `ca`,`crt`,and `key` to be the Full Path.
   - Make a credential file in the same directory as the `ovpn` config where the first line is the username and the second one is the password
   - Add the following `auth-user-pass <PATH TO CREDENTIAL FILE>` **NOTE** _The path must also be the full path to the credential file_
   - Add `group openvpn` as this will be needed with iptables
   - Edit the `runner.sh` with the path to your `openvpn.ovpn` file

2. Edit the `virustotal/posttorrent.sh` by adding your API key at the top [Virustotal API Key](https://www.virustotal.com/gui/join-us)

3. Run the `setup.sh` Script this will run you through most of the installation in an automated manner

4. When promted with a large list of env variables for transmission edit the following:

   - `download_dir` Change to your perfered Save path
   - `rpc-password` Change to local password (NOTE: Transmission will automatically hash for you)
   - `rpc-username` Change to a prefered usedname
   - `rpc-whitelist-enabled` Add your localhost ex: `192.168.*.*`
   - `script-torrent-done-enabled` Change this to true
   - `script-torrent-done-filename` Change this to point to `/var/lib/scripts/posttorrent.sh`

5. In the Nginx config that has now opened change the following:

   - Set the `root` to point to the transmission `download_dir` specified in step 2
   - add `autoindex on;` under the `location / {`

6. Add a [tracker](https://torguard.net/checkmytorrentipaddress.php) to display IP address to make sure everything is operational

# Usage

Tranmission will be availabel under the path specified in step 2 of the installation by default it will be under `<IP OF MACHINE>:9091/transmission/` and the malware logs will be viewable under `<IP OF MACHINE>`

# Optional

### Setting up Monitoring

- Install [Rpi-Monitor](https://github.com/XavierBerger/RPi-Monitor) To view resource usage and temps on `<IP OF MACHINE>:8888`
- Configs present in `rpimonitor` folder:
  - `sdcard` Shows Storage Server
  - `network` Shows Tunnel traffic and IP address

### Ease of Navigation

- Allow NGINX to reverse proxy to all 3 paths using the config present under the `nginx` folder:
  - `/monitor` Redirects to Rpi-Monitor
  - `/files` Redirect to file navigation and Scan navigation.
  - `/torrent` Redirect to Transmission webui
  - `/` Redirect to Monitor.
