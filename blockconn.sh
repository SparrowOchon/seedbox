#!/bin/bash

# Allow loopback device (internal communication)
iptables -A INPUT -i lo -j ACCEPT
iptables -A OUTPUT -o lo -j ACCEPT

# Allow all local traffic
iptables -A INPUT -s 192.168.0.0/24 -j ACCEPT
iptables -A OUTPUT -d 192.168.0.0/24 -j ACCEPT

# Allow VPN establishment
iptables -A OUTPUT -j ACCEPT -m owner --gid-owner openvpn

# Allow established connections
iptables -A INPUT -j ACCEPT -m state --state ESTABLISHED

# Accept all tun0 (VPN tunnel) connections
iptables -A OUTPUT -o tun+ -j ACCEPT
iptables -A INPUT -i tun+ -j ACCEPT

# Allow for nslookup to not throw an error
ip6tables -I OUTPUT 1 -p udp -s 0000:0000:0000:0000:0000:0000:0000:0001 -d 0000:0000:0000:0000:0000:0000:0000:0001 -j ACCEPT

# Drop everything else (ipv4)
iptables -P INPUT DROP
iptables -P OUTPUT DROP
iptables -P FORWARD DROP

# Drop everything (ipv6) <WE DISABLE IPV6>
ip6tables -P INPUT DROP
ip6tables -P OUTPUT DROP
ip6tables -P FORWARD DROP
