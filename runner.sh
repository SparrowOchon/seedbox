
#!/bin/bash
#   By Network Silence

#
#   The following is run by the checkvpn.sh init process
#
#
ovpn_file_to_use="/home/ubuntu/vpn/openvpn.ovpn"
scripts_path="/var/lib/scripts/"

function killtransmission {
        while true ; do
                if [ "$(ip addr | grep tun0)" == "" ]; then
                        systemctl stop transmission-daemon
                        iptables -F
                        break;
                fi
                sleep 30s
        done

}

/usr/sbin/openvpn --config "$ovpn_file_to_use" &
sleep 30s # Let VPN Setup
#systemctl start rpimonitor
while true ; do
        if ! [ "$(ip addr | grep tun0)" == "" ]; then
                $scripts_path/blockconn.sh # Enable IPTables for Added Security
                systemctl start transmission-daemon
                killtransmission
        else
                sleep 30s
        fi
done
