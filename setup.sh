#!/bin/bash

PASS="" #Password of sudoer to avoid asking multiple times
function get_sudoer_pass(){
	while [[ $PASS == "" ]]; do
		sudo -k
		echo -n "Enter the sudo password:"
		read temppass
		echo $temppass | sudo -S echo testpass &> /dev/null
		if ! [ "$(sudo -n echo testpass 2>&1)" == "testpass" ]; then
			echo "Incorrect password was entered"
		else
			PASS=$temppass
		fi
	done
}
function print_error(){
	echo "USAGE: ./$0"
	exit 1
}

# Download Transmission
echo $PASS | sudo -S apt-get update -y
echo $PASS | sudo -S apt-get install transmission-daemon -y
echo $PASS | sudo -S apt-get install golang -y
echo $PASS | sudo -S apt-get install nginx -y
echo $PASS | sudo -S apt-get install openvpn -y
echo $PASS | sudo -S groupadd -r openvpn

# Edit Settings.json
echo $PASS | sudo -S systemctl stop transmission-daemon
echo $PASS | sudo -S vim /var/lib/transmission-daemon/info/settings.json # Allow settings to be edited 

# Setup Exec Scripts
echo $PASS | sudo -S mkdir -p /var/lib/scripts
echo $PASS | sudo -S cp virustotal/posttorrent.sh /var/lib/scripts/posttorrent.sh
echo $PASS | sudo -S chown debian-transmission /var/lib/scripts/posttorrent.sh
echo $PASS | sudo -S chgrp debian-transmission /var/lib/scripts/posttorrent.sh
echo $PASS | sudo -S chmod 755 /var/lib/scripts/posttorrent.sh
echo $PASS | sudo -S cp runner.sh /var/lib/scripts/runner.sh
echo $PASS | sudo -S chmod 755 /var/lib/scripts/runner.sh
echo $PASS | sudo -S cp blockconn.sh /var/lib/scripts/blockconn.sh
echo $PASS | sudo -S chmod 755 /var/lib/scripts/blockconn.sh

# Setup Virus Scanner
echo $PASS | sudo -S go get -d github.com/VirusTotal/vt-cli/vt
echo $PASS | sudo -S make install ~/go/src/github.com/VirusTotal/vt-cli
# Make sure VirusTotal Script is in PATH
echo "PATH=$PATH:/home/$USER/go/bin" >> ~/.bashrc

# Set static DNS to not be ISP
echo $PASS | sudo -S rm /etc/resolv.conf
echo $PASS | sudo -S echo "nameserver 1.1.1.1" > /etc/resolv.conf
echo $PASS | sudo -S echo "nameserver 1.0.0.1" >> /etc/resolv.conf
echo $PASS | sudo -S chmod 544 /etc/resolv.conf

# Disable IPv6 Prevent Leaks
echo $PASS | sudo -S echo "net.ipv6.conf.default.disable_ipv6=1" >> /etc/sysctl.conf
echo $PASS | sudo -S echo "net.ipv6.conf.all.disable_ipv6=1" >> /etc/sysctl.conf
echo $PASS | sudo -S echo "net.ipv6.conf.lo.disable_ipv6=1" >> /etc/sysctl.conf
echo $PASS | sudo -S sysctl -p

# Disable Systemd-Resolved
echo $PASS | sudo -S systemctl disable systemd-resolved.service
echo $PASS | sudo -S systemctl stop systemd-resolved

# Move KillSwitch and enable it
echo $PASS | sudo -S mv checkvpn.service /etc/systemd/system/
echo $PASS | sudo -S systemctl enable checkvpn

# Open NGINX config

echo $PASS | sudo -S systemctl stop nginx
echo $PASS | sudo -S nano /etc/nginx/sites-enabled/default

# Restart System
echo $PASS | sudo -S reboot -h now
