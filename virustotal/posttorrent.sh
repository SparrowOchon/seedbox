#!/bin/bash

VIRUSTOTAL_API="<API KEY>"
TR_DOWNLOADED_PATH="$TR_TORRENT_DIR/$TR_TORRENT_NAME"

function isbinary() {
  LC_MESSAGES=C grep -Hm1 '^' < "${1-$REPLY}" | grep -q '^Binary'
}
function scan_file {
	local malware_file=$(printf %q "$file_name")
	local storage_file="${malware_file##*/}"
	local upload_hash=$(md5sum "$file_name" | cut -d' ' -f1)
	storage_file="${storage_file%.*}"
	/home/ubuntu/go/bin/vt scan file "$malware_file" -k "$VIRUSTOTAL_API"
	/home/ubuntu/go/bin/vt file "$upload_hash" --include=last_analysis_results.*.result,last_analysis_results.*.category -k "$VIRUSTOTAL_API" > "$TR_TORRENT_DIR/Scans/$storage_file.txt"
}
function scan_dir {
	find "$TR_DOWNLOADED_PATH" -type f -print0  | while IFS= read -rd '' file_name; do 
		# ... rest of the loop body
		if [[ 7z t "$file_name" || isbinary "$file_name" ]]; then
		    scan_file "$file_name"
		fi
	done
}

if [[ -d "$TR_DOWNLOADED_PATH" ]]; then
	scan_dir
elif [[ -f "$TR_DOWNLOADED_PATH" ]]; then
	if [[ 7z t "$TR_DOWNLOADED_PATH" || isbinary "$TR_DOWNLOADED_PATH" ]]; then
    	scan_file "$TR_DOWNLOADED_PATH"
    fi
else
    echo "Invalid file"
    exit 1
fi
